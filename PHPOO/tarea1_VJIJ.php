<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Piramid </title>
  </head>

  <style >

    .container{
      margin: auto;
      width: 500px;
    }

  </style>

  <body>
    <header>
    </header>
    <div class="container">
      <section class="piramide" >
        <h1> Pirámide </h1>
        <?php
        for($i=1;$i<=30;$i++){
          for ($e=0; $e <=30-$i  ; $e++)
            echo "&nbsp ";
            for ($e=0; $e <(2*$i)-1 ; $e++)
              echo "*";
              echo "<br>";
        }
     ?>

           </section>

           <section class="rombo">
             <h1> Rombo </h1>
             <?php
             for($i=1;$i<=30;$i++){
               for ($e=0; $e <=30-$i  ; $e++)
                 echo "&nbsp ";
                 for ($a=0; $a <(2*$i)-1 ; $a++)
                   echo "*";
                   echo "<br>";
             }
             for($i=30-2;$i>=0;$i--){
               for ($e=0; $e <=30-$i-1  ; $e++)
                 echo "&nbsp ";
                 for ($a=0; $a <(2*$i)+1 ; $a++)
                   echo "*";
                   echo "<br>";
             }
              ?>
           </section>
        </div>
      </main>
      <footer></footer>
    </div>

  </body>
</html>
