<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Registro </title>
    <link rel="stylesheet" href="estilo.css">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, shrink-to-feet=no" />
  </head>
  <body>
    <div class="contenedor">

      <header>
        <h1> Registro de alumno </h1>
      </header>
      <hr>

      <main>

        <form class="" action="index.html" method="post">
          <section>
            <article class="log">
              <label for=""> No. de cuenta: </label>
              <input type="text"class="campos"  name="" value="">
              <br>
              <label for=""> Nombre: </label>
              <input type="text" class="campos" name="" value="">
              <br>
              <label for=""> Primer apellido: </label>
              <input type="text"class="campos"  name="" value="">
              <br>
              <label for=""> Segundo apellido: </label>
              <input type="text"class="campos"  name="" value="">
              <br>
              <label for=""> Contraseña </label>
              <input type="password"class="campos"  name="" value="">
              <br>
              <label for=""> Genero </label><br>
              <label for=""> Masculino </label>
              <input type="radio" name="" value="M">
              <br>
              <label for=""> Femenino </label>
              <input type="radio" name="" value="">
              <br>
              <label for=""> Otro </label>
              <input type="radio" name="" value="">
              <br>
              <label for=""> Fecha de nacimiento </label>
              <input type="date" class="campos" name="" value="">
              <br><br>
              <input type="submit" class="boton" value="Enviar">
            </article>
          </section>
        </form>
      </main>

    </div>

  </body>
</html>
