<!DOCTYPE html>
<html lang="else" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Formulario </title>
    <link rel="stylesheet" href="estilo.css">
  </head>
  <body>
    <div class="contenedor">
      <header>
        <h1> Login </h1>
      </header>
      <hr>

      <main>

        <form class="" action="index.html" method="post">
          <section>
            <article class="log">
              <label for="nocta"> Número de cuenta: </label> <br>
              <input type="text" class="campos" name="nocta" value="">
              <br>
              <label for="contraseña"> Contraseña: </label><br>
              <input type="password" class="campos" name="contraseña" value="">
              <br>
              <br>
              <input type="submit" class="boton" value="Enviar" >
            </article>
          </section>
        </form>

      </main>

    </div>

  </body>
</html>
