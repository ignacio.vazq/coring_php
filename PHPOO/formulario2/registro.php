<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Registo </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  </head>
  <body>
    <h1>Registro alumno</h1>
    <hr>
  <div class="container">
    <form action="insert.php" method="post">
      <div class="form-group">
        <label for="formGroupExampleInput"> Id Alumno </label>
        <input type="text" class="form-control" id="insert_id" name="insert_id" placeholder="Id">
      </div>

      <div class="form-group">
        <label for="formGroupExampleInput2"> Número de cuenta </label>
        <input type="text" class="form-control" id="insert_numcta" name="insert_numcta" placeholder="Número de cuenta">
      </div>

      <div class="form-group">
        <label for="formGroupExampleInput3"> Nombre </label>
        <input type="text" class="form-control" id="insert_nombre" name="insert_nombre" placeholder="Nombre">
      </div>

      <div class="form-group">
        <label for="formGroupExampleInput4"> Apellido 1 </label>
        <input type="text" class="form-control" id="insert_apellido1" name="insert_apellido1" placeholder="Apellido 1">
      </div>

      <div class="form-group">
        <label for="formGroupExampleInput5"> Apellido 2 </label>
        <input type="text" class="form-control" id="insert_apellido2" name="insert_apellido2" placeholder="Apellido 2">
      </div>b

      <label for=""> Genero </label> <br>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="insert_genero" id="insert_genero" value="H">
        <label class="form-check-label" for="inlineRadio1"> Hombre </label>
      </div>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="insert_genero" id="insert_generoM" value="M">
        <label class="form-check-label" for="inlineRadio2"> Mujer </label>
      </div>

      <div class="form-group">
        <label for="formGroupExampleInput5"> Fecha de nacimiento </label>
        <input type="text" class="form-control" id="insert_fechaNac" name="insert_fechaNac" placeholder="aaaa/mm/dd">
      </div>

      <button type="submit" name="insertar" class="btn btn-info"> Agregar alumno </button>
      <a class="btn btn-secondary" href="index.php" role="button"> Ver registros </a>

    </form>

  </div>

  </body>
</html>
