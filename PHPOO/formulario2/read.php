<?php
include('conn.php');

class registro{
  public function listar(){
    try {
      $conexion = new conexion();
      $con = $conexion -> get_conexion();

      $sql = $con -> query("SELECT * FROM cursosql.alumno");
      $alumno = $sql -> fetchAll(PDO::FETCH_OBJ);

      return $alumno;

    } catch (Exception $e) {
      die(GetMessage());
    }finally{
      //$dbh = null;
    }
  }

  public function insertar($alumno_id, $al_numcta, $al_nombre, $al_apellido1, $al_apellido2, $al_genero, $al_fechaNac){
      $conexion = new conexion();

      $con = $conexion -> get_conexion();

      $sql = $con -> prepare('INSERT INTO cursosql.alumno (alumno.alumno_id, alumno.al_numcta, alumno.al_nombre, alumno.al_apellido1, alumno.al_apellido2, alumno.al_genero, alumno.al_fechaNac)
      VALUES (:alumno_id, :al_numcta, :al_nombre, :al_apellido1, :al_apellido2, :al_genero, :al_fechaNac)');
      $sql -> bindParam(':alumno_id', $alumno_id, PDO :: PARAM_INT);
      $sql -> bindParam(':al_numcta', $al_numcta, PDO :: PARAM_STR);
      $sql -> bindParam(':al_nombre',$al_nombre,PDO :: PARAM_STR);
      $sql -> bindParam(':al_apellido1', $al_apellido1, PDO :: PARAM_STR);
      $sql -> bindParam(':al_apellido2', $al_apellido2, PDO :: PARAM_STR);
      $sql -> bindParam(':al_genero', $al_genero, PDO :: PARAM_STR);
      $sql -> bindParam(':al_fechaNac', $al_fechaNac, PDO :: PARAM_STR);

      // $sql -> execute();

      if($sql -> execute()){
        return header('Location: index.php');
      }else{
        return "Error";
      }
  }
}
