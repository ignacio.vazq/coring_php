<?php
  include('read.php');
  $instance = new registro();
  $alumno = $instance -> listar();
 ?>

<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Lista registros </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-12"> <h1> Registros de tabla alumno </h1> </div>
        <div class="col-12">
          <a href="registro.php"><button class="btn btn-primary" >  Agregar Alumno </button></a>
        </div>
        <div class="col-12">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Numero de cuenta </th>
                <th> Nombre </th>
                <th> Apellido 1 </th>
                <th> Apellido 2 </th>
                <th> Genero </th>
                <th> Fecha de nacimiento </th>
              </tr>
            </thead>

            <tbody>
              <?php foreach($alumno as $registro): ?>
                <tr>
                  <td> <?php echo $registro -> alumno_id ?> </td>
                  <td> <?php echo $registro -> al_numcta ?> </td>
                  <td> <?php echo $registro -> al_nombre ?> </td>
                  <td> <?php echo $registro -> al_apellido1 ?> </td>
                  <td> <?php echo $registro -> al_apellido2 ?> </td>
                  <td> <?php echo $registro -> al_genero ?> </td>
                  <td> <?php echo $registro -> al_fechaNac ?> </td>


                </tr>
              <?php endforeach; ?>

            </tbody>

      </div>

    </div>

  </body>
</html>
