<?php
/*
Investigar que hacen las siguientes funciones y un ejemplo sencillo de cada una de ellas
    FUNCIONES PREDEFINIDAS DE PHP
1. substr
    devuelve parte de una cadena
      sintaxis
    substr ( string $string , int $start [, int $length ] ) : string
      ejemplo
    echo substr('abcdef', 1);   devuelve,  bcdef

2. strstr
    Encuentra la primera aparición de un $string, esta función en sensible a mayúsculas
     sintaxis
    strstr ( string $haystack , mixed $needle [, bool $before_needle = FALSE ] ) : string
     ejemplo
    $correo = 'name@gmail.com';
    $dominio = strstr($correo.'@');
    echo $dominio;  devuelve @gmail.com

3.strpos
    Encuentra la posición de la primera pcurrencia de un substring en un string
      sintaxis
    strpos ( string $haystack , mixed $needle [, int $offset = 0 ] ) : mixed
    encuentra la posición numérica de la primera ocurrencia de needle(aguja) en el string(pajar).
      ejemplo
    $pajar = 'abc';
    $aguja = 'a';
    $pos = srtpos($pajar,$aguja):
    if ($pos !== false) {
     echo "La cadena '$aguja' fue encontrada en la cadena '$pajar'";
         echo " y existe en la posición $pos";
       } else {
     echo "La cadena '$aguja' no fue encontrada en la cadena '$pajar'";
      }

4. implode
    Une elementos de un array en un string
      sintaxis
    implode ( string $glue , array $pieces ) : string
    une elementos de un array en un string con glue(pegamento)
      ejemplo
    $array = array('apellido', 'email', 'teléfono');
    $separado_por_comas = implode(",", $array);
    echo $separado_por_comas;  devuelve apellido,email,teléfono

5. explode
    Divide un string en varios strings
      sintaxis
    explode ( string $delimiter , string $string [, int $limit = PHP_INT_MAX ] ) : array
    Devuelve un array de string, siendo cada uno un substring del parámetro string
    formado por la división realizada por los delimitadores indicados en el parámetro delimiter.
      ejemplo
    $pizza  = "porción1 porción2 porción3 porción4 porción5 porción6";
    $porciones = explode(" ", $pizza);
    echo $porciones[0]; devuelve porción1
    echo $porciones[1]; devuelve porción2
    echo $porciones[2]; devuelve porción3
    echo $porciones[3]; devuelve porción4
    echo $porciones[4]; devuelve porción5
    echo $porciones[5]; devuelve porción6

6. utf8_encode
    Codifica un string ISO-8859-1 a UTF-8
      sintaxis
    utf8_encode ( string $data ) : string
    Ésta función codifica el string data a UTF-8, y devuelve una versión codificada.
    UTF-8 es un mecanismo estándar usado por Unicode para la codificación de los valores
    de caracteres anchos en un flujo de bytes.


7. utf8_decode
    Convierte una cadena con los caracteres codificados ISO-8859-1 con UTF-8 a un sencillo byte ISO-8859-1
    sintaxis
    utf8_decode ( string $data ) : string
    Ésta función decodifica los $data, asumidos a ser codificados por UTF-8 a ISO-8859-1

    NOTA: ISO-8859-1 es una norma de la ISO que define la codificación del alfabeto latino, permitiendo
    el uso de caracteres especiales

8. array_pop
    Extrae el último elemento del final del array
      sintaxis
    array_pop ( array &$array ) : mixed
    extrae y devuelve el último valor del array, acortando el array con un elemento menos.
      ejemplo
    $frutero = array("naranja", "plátano", "manzana", "frambuesa");
    $fruta = array_pop($frutero);
    print_r($frutero);
    Despues de esto, el array solo tendra 3 elementos
    [0] => naranja
    [1] => plátano
    [2] => frambuesa

9. array_push
    Inserta uno o más elemtos al final de un array
      sintaxis
    array_push ( array &$array , mixed $value1 [, mixed $... ] ) : int
    trata array como si fuera una pila y coloca la variable que se le proporciona
    al final del array. El tamaño del array será incrementado por el número de variables insertados.
      ejemplo
    $pila = array("naranja", "plátano");
    array_push($pila, "manzana", "arándano");
    print_r($pila);
    Despues de esto, al array $pila se le agregan 2 elementos([2] y [3])
    [0] => naranja
    [1] => plátano
    [2] => manzana
    [3] => arándano


10. array_diff
    Calcula la diferencia entre arrays
      sintaxis
    array_diff ( array $array1 , array $array2 [, array $... ] ) : array
    compara array1 con uno o más arryas y devuelve los valores de array1 que no estén
    presentes en ninguno de los otros arrays.
      ejemplo
    $array1    = array("a" => "green", "red", "blue", "red");
    $array2    = array("b" => "green", "yellow", "red");
    $resultado = array_diff($array1, $array2);
    print_r($resultado);
    En esta salida nos mostrará [1] => blues, ya que al compararlos ese elemento es el que sobra

11. array_walk
    Aplica una función proporcionada por el usuario a cada miembro de un array
      sintaxis
    array_walk ( array &$array , callable $callback [, mixed $userdata = NULL ] ) : bool
    Aplica la funcion definida por el usuario dada por callback a cada elemento del array
      ejemplo
    $frutas = array("d" => "limón", "a" => "naranja", "b" => "banana", "c" => "manzana");
    function test_alter(&$elemento1, $clave, $prefijo)
    {
        $elemento1 = "$prefijo: $elemento1";
    }

    function test_print($elemento2, $clave)
    {
        echo "$clave. $elemento2<br />\n";
    }

    echo "Antes ...:\n";
    array_walk($frutas, 'test_print');

    array_walk($frutas, 'test_alter', 'fruta');
    echo "... y después:\n";

    array_walk($frutas, 'test_print');

12. sort
    Ordena un array
      sintaxis
    sort ( array &$array [, int $sort_flags = SORT_REGULAR ] ) : bool
    esta función ordena un array. Los elementos estarán ordenados de menor a mayor
    cuando la función haya terminado
      ejemplo
    $frutas = array("limón", "naranja", "banana", "albaricoque");
    sort($frutas);
    foreach ($frutas as $clave => $valor) {
        echo "frutas[" . $clave . "] = " . $valor . "\n";
    }
    La salida de esta funcion seria
    frutas[0] = albaricoque
    frutas[1] = banana
    frutas[2] = limón
    frutas[3] = naranja

13. current
    Devuelve el elemento actual en un array
      sintaxis
    current ( array &$array ) : mixed
    Cada array tiene un puntero interno a su elemento "actual", que es indicado desde
    el primer elemento insertado en el array.

14. date
    Dar formato a la fecha/hora local
      sintaxis
    date ( string $format [, int $timestamp = time() ] ) : string
    devuelve una cadena formateada segun el formato dado usando el parámetro de tipo integer timestamp
    dado o el momento actual si no se da una marca de tiempo. timestap es opcional y por defecto es el valor de time()
      ejemplo
    // Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
    date_default_timezone_set('UTC');

    // Imprime algo como: Monday
    echo date("l");

    // Imprime algo como: Monday 8th of August 2005 03:12:46 PM
    echo date('l jS \of F Y h:i:s A');

    // Imprime: July 1, 2000 is on a Saturday
    echo "July 1, 2000 is on a " . date("l", mktime(0, 0, 0, 7, 1, 2000));


15. empty
    Determina si una variable está vacíarray
      sintaxis
    empty ( mixed $var ) : bool
      ejemplo
    if (empty($var)) {
        echo '$var es o bien 0, vacía, o no se encuentra definida en absoluto';
    }

16. isset
    Determina si una variable está definida y no es NULL
      sintaxis
    isset ( mixed $var [, mixed $... ] ) : bool
      ejemplo
    // Esto evaluará a TRUE así que el texto se imprimirá.
    if (isset($var)) {
        echo "Esta variable está definida, así que se imprimirá";
    }

17. serialize
    Genera una representación apta para el almacenamiento de un valor
      sintaxis
    serialize ( mixed $value ) : string
    Genera una representación almacenable de un valor.
    es útil para el almacenamiento de valores en PHP sin perder su tipo y estructura
      ejemplo
     $datos_sesion contiene un array multi-dimensional con
     información del usuario actual. Usamos serialize() para
     almacenarla en una base de datos al final de la petición.

    $con  = odbc_connect("bd_web", "php", "gallina");
    $sent = odbc_prepare($con,
          "UPDATE sesiones SET datos = ? WHERE id = ?");
    $datos_sql = array (serialize($datos_sesion), $_SERVER['PHP_AUTH_USER']);

    if (!odbc_execute($sent, &$datos_sql)) {
        $sent = odbc_prepare($con,
         "INSERT INTO sesiones (id, datos) VALUES(?, ?)");
        if (!odbc_execute($sent, &$datos_sql)) {
             Algo ha fallado..
        }
    }

18. unserialize
    Crea un valor PHP a partir de una representación almacenada
      sintaxis
    unserialize ( string $str [, array $options ] ) : mixed
    unserialize() toma una única variable serializada y la vuelve a convertir a un valor de php
      ejemplo
    $objeto_seriado='O:1:"a":1:{s:5:"valor";s:3:"100";}';

    ini_set('unserialize_callback_func', 'mi_llamada_de_retorno'); // defina su callback_function

    function mi_llamada_de_retorno($nombre_clase)
    {
        // tan solo incluya un fichero que contenga su definición de clase
        // recibe $nombre_clase para determinar qué definición de clase requiere
    }


*/
 ?>
